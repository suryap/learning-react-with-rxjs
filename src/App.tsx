import logo from './logo.svg';
import React from 'react';
import { UserList } from './components/user-list';

function App() {
  return (
    <div className="App">
      <UserList />
    </div>
  );
}

export default App;
