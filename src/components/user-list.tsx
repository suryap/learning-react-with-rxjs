import { Button, Typography } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import React, { useCallback, useEffect, useState } from 'react';
import { from, Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import {
  listUserRepositories,
  listUsersService
} from '../services/user.service';

export const User = ({
  user,
  getRepositories
}: {
  user: any;
  getRepositories: Function;
}) => {
  return (
    <Grid container spacing={2} justify='space-between'>
      <Grid item>
        <img src={user.avatar_url} style={{ width: 50 }} />
      </Grid>
      <Grid item>{user.login}</Grid>
      <Grid item>
        <Button
          size='small'
          color='primary'
          onClick={() => getRepositories(user)}
        >
          View Repositories
        </Button>
      </Grid>
    </Grid>
  );
};

export const UserList = () => {
  const destroy$ = new Subject();

  const [users, setUsers] = useState([]);
  const [userRepositories, setUserRepositories] = useState([]);

  useEffect(() => {
    listUsersService()
      .pipe(first(), takeUntil(destroy$))
      .subscribe((users: any) => {
        setUsers(users);
      });

    return () => {
      destroy$.next();
      destroy$.complete();
    };
  }, []);

  const getRepositories = useCallback((activeUser) => {
    listUserRepositories(activeUser.repos_url)
      .pipe(first(), takeUntil(destroy$))
      .subscribe((response: any) => {
        setUserRepositories(response);
      });
  }, []);

  return (
    <Container>
      <Grid container spacing={2} justify='space-between'>
        <Grid item>
          {users &&
            users.map((user, i) => (
              <div key={i} style={{ padding: 20 }}>
                <User user={user} getRepositories={getRepositories} />
              </div>
            ))}
        </Grid>
        <Grid item>
          {userRepositories && userRepositories.length > 0 && (
            <Typography variant='h3' gutterBottom>
              Repositories
            </Typography>
          )}
          <div>
            {userRepositories &&
              userRepositories.map((repo: any, i) => {
                return (
                  <Grid container key={i} spacing={2} justify='space-between'>
                    <Grid item>{repo.name}</Grid>
                    <Grid item>{repo.language}</Grid>
                  </Grid>
                );
              })}
          </div>
        </Grid>
      </Grid>
    </Container>
  );
};
