import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

export const listUsersService = () =>
  ajax
    .getJSON(`https://api.github.com/users?per_page=10`)
    .pipe(catchError((err) => of({ error: true, message: err.message })));

export const listUserRepositories = (url: string) =>
  ajax
    .getJSON(url)
    .pipe(catchError((err) => of({ error: true, message: err.message })));
